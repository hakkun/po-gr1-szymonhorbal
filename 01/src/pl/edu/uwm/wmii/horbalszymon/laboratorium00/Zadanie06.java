package pl.edu.uwm.wmii.horbalszymon.laboratorium00;

public class Zadanie06 {

    static char wall = '|', floor = '-', plus = '+';

    public static void main ( String [] args )
    {
        System.out.printf("  ");
        wypisz('/', 5, false);
        System.out.printf(" ");
        wypisz('"', 5, true);

        System.out.printf("(| o o |)\n");
        System.out.printf("  | ^ |\n");
        System.out.printf(" | '-' |\n");
        System.out.printf(" ");
        wypisz('-', 5, true);



    }

    public static void wypisz(char c, int x, boolean between)
    {
        if (between) {
            System.out.printf("%c", plus);
            for (int i = 0; i < x; i++)
                System.out.printf("%c", c);
            System.out.printf("%c", plus);
        }

        else {
            for (int i = 0; i < x; i++)
                System.out.printf("%c", c);
        }
        System.out.printf("\n");

    }

}
